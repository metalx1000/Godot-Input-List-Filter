extends VBoxContainer

export (String) var url = "https://pastebin.com/raw/YbHHxu7p"
export (String) var group = "form"
export (String) var placeholder = "Search"
export (bool) var focus = false

var items = []

onready var input = $LineEdit
onready var list = $Control/ItemList
onready var control = $Control
onready var http = $HTTPRequest

func _ready():
	input.placeholder_text = placeholder
	add_to_group(group)
	#margin_bottom = 200

	http.request(url)
	
	
func show_list():
	#set_v_size_flags(3)
	raise()
	list.select(0)
	list.show()
	

func hide_list():
	#set_v_size_flags(1)
	list.hide()

func select_item(index):
	input.text = list.get_item_text(index).strip_edges()

func activate_item(index):
	select_item(index)
	input.grab_focus()
	hide_list()

func get_list(result, response_code, headers, body):
	items = body.get_string_from_utf8().split("\n")
	if focus:
		input.grab_focus()
	filter_list("")

func filter_list(q):
	q = q.to_upper()
	list.clear()
	for i in items:
		i = i.to_upper()
		if q in i || q == "":
			list.add_item(i)
