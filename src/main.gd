extends Control

onready var submit_btn = $Button
onready var http = $HTTPRequest
var url = "https://filmsbykris.com/scripts/php/echo_json.php"

func submit():
	var inputs = get_tree().get_nodes_in_group("form")
	var u = url + "?"
	for i in inputs:
		u += i.name + "=" + i.input.text + "&"
	
	http.request(u)


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	print(body.get_string_from_utf8())
